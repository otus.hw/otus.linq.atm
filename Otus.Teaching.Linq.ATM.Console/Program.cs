﻿using System;
using System.Linq;
using Otus.Teaching.Linq.ATM.Core.Services;
using Otus.Teaching.Linq.ATM.DataAccess;

namespace Otus.Teaching.Linq.ATM.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Console.WriteLine("Старт приложения-банкомата...");

            var atmManager = CreateATMManager();

            System.Console.WriteLine("1)");
            atmManager.PrintUserInfo("lee", "222");

            System.Console.WriteLine("2)");
            atmManager.PrintAccounts("lee", "222");

            System.Console.WriteLine("3)");
            atmManager.PrintAccountHistory("snow", "111");

            System.Console.WriteLine("4)");
            atmManager.PrintAllInputOperation();

            System.Console.WriteLine("5)");
            atmManager.PrintUserInfo(1000);




            //TODO: Далее выводим результаты разработанных LINQ запросов

            System.Console.WriteLine("Завершение работы приложения-банкомата...");
        }

        static ATMManager CreateATMManager()
        {
            using var dataContext = new ATMDataContext();
            var users = dataContext.Users.ToList();
            var accounts = dataContext.Accounts.ToList();
            var history = dataContext.History.ToList();
                
            return new ATMManager(accounts, users, history);
        }
    }
}