﻿using System;
using System.Collections.Generic;
using System.Linq;
using Otus.Teaching.Linq.ATM.Core.Entities;

namespace Otus.Teaching.Linq.ATM.Core.Services
{
    public class ATMManager
    {
        public IEnumerable<Account> Accounts { get; private set; }
        
        public IEnumerable<User> Users { get; private set; }
        
        public IEnumerable<OperationsHistory> History { get; private set; }
        
        public ATMManager(IEnumerable<Account> accounts, IEnumerable<User> users, IEnumerable<OperationsHistory> history)
        {
            Accounts = accounts;
            Users = users;
            History = history;
        }

        //TODO: Добавить методы получения данных для банкомата
        /// <summary>
        /// Печать информации о пользователе
        /// </summary>
        /// <param name="login"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public void PrintUserInfo(string login, string password)
        {
            var user = (from u in Users
                       where u.Login == login && u.Password == password
                       select u).FirstOrDefault();
            Console.WriteLine($"Info: {user.FirstName} {user.MiddleName} {user.MiddleName} {user.RegistrationDate} {user.PassportSeriesAndNumber} {user.Phone}");
        }

        /// <summary>
        /// Печать информации об аккаунтах
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public void PrintAccounts(string login, string password)
        {
            var accounts = (from u in Users
                            from account in Accounts
                            where u.Login == login && u.Password == password
                            where account.UserId == u.Id
                            select new
                            {
                                u.FirstName, u.SurName, account
                            }
                            ).ToList();

            Console.WriteLine(string.Join(Environment.NewLine, accounts.Select(x => $"User: {x.FirstName} {x.SurName}" ).Distinct()  ));
            Console.WriteLine(string.Join(Environment.NewLine,
                accounts.Select(x => $"Номер: {x.account.Id} Дата открытия: {x.account.OpeningDate} Остаток: {x.account.CashAll}")));
        }
        /// <summary>
        /// Печать аккаунтов и истории к ним.
        /// </summary>
        /// <param name="login"></param>
        /// <param name="password"></param>
        public void PrintAccountHistory(string login, string password) 
        {
            var user = (from u in Users
                        where u.Login == login && u.Password == password
                        select u).FirstOrDefault();

            var hystory = (from a in Accounts
                           join h in History on a.Id equals h.AccountId into j1
                           from j2 in j1.DefaultIfEmpty()
                           where a.UserId == user.Id
                           
                           group j2 by a.Id);

            Console.WriteLine($"User: {user.FirstName} {user.SurName}");

            foreach (var acc in hystory)
            {
                Console.WriteLine($"Accoun id {acc.Key}");
                foreach (var hist in acc)
                {
                    Console.WriteLine($"{hist.OperationDate} {hist.OperationType} {hist.CashSum}");
                }
            }
        }

        /// <summary>
        /// Печать всех операций пополнения счета
        /// </summary>
        public void PrintAllInputOperation()
        {
            var inputOperations = (from h in History
                                   join a in Accounts on h.AccountId equals a.Id
                                   join u in Users on a.UserId equals u.Id
                                   where h.OperationType == OperationType.InputCash

                                   select new
                                   {
                                       h.AccountId, h.OperationDate, h.OperationType, u.FirstName, u.SurName
                                   }
                                   ).DefaultIfEmpty();

            Console.WriteLine(string.Join(Environment.NewLine,
                inputOperations.Select(x => $"{x.AccountId} {x.OperationDate} {x.OperationType} {x.FirstName} {x.SurName}")));
        }

        /// <summary>
        /// Печать пользователей с балансом больше N
        /// </summary>
        /// <param name="n"></param>
        public void PrintUserInfo(int n)
        {
            var users = (from u in Users
                         from account in Accounts
                         where account.CashAll > n
                         select u).Distinct().DefaultIfEmpty();

            Console.WriteLine(string.Join(Environment.NewLine,
                users.Select(x => $"{x.FirstName} {x.SurName} ")));
        }
    }
}